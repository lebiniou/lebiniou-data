# Introduction

This repository contains data files for use with [lebiniou](https://gitlab.com/lebiniou/lebiniou).

It is highly recommended to install them.

# Installation

  1. Configure

  ```sh
  $ autoreconf -i
  $ ./configure
  ```

  2. Compile and install

  ```sh
  $ make
  $ sudo make install
  ```
